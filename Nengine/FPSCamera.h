#pragma once
#include <iostream>
#include <GL/glew.h>
#include <GLFW\glfw3.h>
#include "ShaderManager.h"
#include "Math.h"

using namespace std;

const static float cam_speed = 15.0f;
const static double mouse_sensitivity_yaw = 1.0f;
const static double mouse_sensitivity_pitch = 3.0f;

class FPSCamera
{
public:
	static void glfw_key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
	static void glfw_cursor_callback(GLFWwindow* window, double xpos, double ypos);
	static double pitch, yaw;
	FPSCamera(GLFWwindow * window, float aspect_ratio, vec3 world_position, vec3 lookat);
	~FPSCamera();
	void update(double time_delta);

private:
	static bool w_key, s_key, a_key, d_key;
	static int xmov, zmov;

	void rotate_x(float factor);
	void rotate_y(float factor);
	void FPSCamera::movex(float xmmod);
	void FPSCamera::movez(float zmmod);
	
	vec3 position;
	versor rotation_versor;
	GLFWwindow* window;

	//shader program
	GLuint program;

	//matrices
	mat4 projection_matrix;

	//uniforms
	GLuint view_mat_loc;
	GLuint model_mat_loc;
	GLuint proj_mat_loc;
};

