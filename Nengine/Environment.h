#pragma once
#include <iostream>
#include <string>
#include <GL\glew.h>
#include <GLFW\glfw3.h>
#include "Logger.h"
#include "ShaderManager.h"
#include "Triangle.h"
#include "FPSCamera.h"

void glfw_window_size_callback(GLFWwindow* window, int width, int height);
static int window_width;
static int window_height;

class Environment
{
public:
	Environment(GLFWwindow* window, int width, int height);
	~Environment();
	void start();

private:
	void loop();
	void render();
	void update(double time_delta);
	void update_fps_counter(double current_time);
	GLFWwindow* window;

	//time variables
	double t;
	const double dt = 0.01;

	//other
	Logger* logger;
	ShaderManager* sh_manager;
	float aspect_ratio;

	//game objects
	Triangle *tri1, *tri2, *tri3;
	FPSCamera* camera;

};

