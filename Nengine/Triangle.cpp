#include "Triangle.h"


Triangle::Triangle()
{
}

Triangle::Triangle(float width, float height, const vec3 & world_position)
{
	this->width = width;
	this->height = height;
	program = ShaderManager::getInstance()->getShaderProgram("program_default");
	this->model_mat_loc = glGetUniformLocation(program, "model");
	this->model_mat = translate(identity_mat4(), world_position);

	float position[] = {
		0.0f, height, 0.0f,
		-width / 2, 0.0f, 0.0f,
		width / 2, 0.0f, 0.0f
	};

	float color[] = {
		1.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 1.0f
	};

	unsigned int indices[] = { 0, 1, 2 };
	number_elements = sizeof(indices) / sizeof(unsigned int);

	GLuint points_vbo = 0;
	glGenBuffers(1, &points_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, points_vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(position), position, GL_STATIC_DRAW);

	GLuint color_vbo = 0;
	glGenBuffers(1, &color_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, color_vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(color), color, GL_STATIC_DRAW);

	GLuint index_vbo = 0;
	glGenBuffers(1, &index_vbo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_vbo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	vao = 0;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, points_vbo);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, color_vbo);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_vbo);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

}

void Triangle::render()
{
	glUseProgram(program);
	glBindVertexArray(vao);
	glUniformMatrix4fv(model_mat_loc, 1, GL_FALSE, model_mat.m);
	glDrawElements(GL_TRIANGLES, number_elements, GL_UNSIGNED_INT, (void*)0);
}

Triangle::~Triangle()
{
}
