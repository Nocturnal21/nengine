#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <fstream>
#include <iostream>
#include <GL\glew.h>
#include <map>
#include "Logger.h"

using namespace std;

class ShaderManager {
public:
	static ShaderManager* getInstance(void);
	const GLuint getShaderProgram(std::string program_name);
private:
	ShaderManager();
	void load_shaders(void);
	void link_shaders(void);
	bool read_shader(const string &dir, string *shader_src);
	void _print_shader_info_log(GLuint shader_index);
	void _print_program_info_log(GLuint program_index);

	static ShaderManager* instance;
	std::map<std::string, GLuint> shader_map;
	std::map<std::string, GLuint> program_map;

	Logger* logger;
};