#pragma once
#include <iostream>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <string>
#include "Environment.h"
#include "Logger.h"

using namespace std;

static int g_gl_width = 640;
static int g_gl_height = 480;
static float aspect_ratio = 0.0f;
static double elapsed_seconds = 0;
static Logger* logger;

int main(int argc, char** argv);
GLFWwindow* init(const int argc, char** argv);
void glfw_error_callback(int error, const char* description);

int main(int argc, char** argv)
{
	logger = Logger::getInstance();
	GLFWwindow* window = init(argc, argv);
	if (window == NULL) {
		cout << "ERROR: Could not create GLFW window!" << endl;
		return EXIT_FAILURE;
	}

	glClearColor(0.3f, 0.3f, 0.3f, 1.0f);

	logger->gl_log_params();

	//start new game environment
	Environment* env = new Environment(window, g_gl_width, g_gl_height);
	env->start();

	glfwTerminate();
	return EXIT_SUCCESS;
}

GLFWwindow* init(const int argc, char** argv) {
	GLFWwindow* window;

	//start GL context and O/S window using GLFW helper
	//Register Error Callback
	glfwSetErrorCallback(glfw_error_callback);
	if (!glfwInit()) {
		cout << "ERROR: Couldn't start GLFW3" << endl;
		return NULL;
	}

	//Anti Aliasing x4
	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	//Window Fullscreen and resolution
	GLFWmonitor* mon = glfwGetPrimaryMonitor();
	const GLFWvidmode* vmode = glfwGetVideoMode(mon);
	g_gl_width = vmode->width;
	g_gl_height = vmode->height;
	aspect_ratio = (float)g_gl_width / (float)g_gl_height;
	window = glfwCreateWindow(vmode->width, vmode->height, "Extended GL Init", mon, NULL);
	logger->gl_log("Creating window: " + to_string(g_gl_width) + "x" + to_string(g_gl_height));

	if (!window) {
		return NULL;
	}

	glfwMakeContextCurrent(window);

	//setting window size callback
	glfwSetWindowSizeCallback(window, glfw_window_size_callback);

	//Start GLEW extension handler
	glewExperimental = GL_TRUE;
	glewInit();

	//get version info
	const GLubyte* renderer = glGetString(GL_RENDERER);
	const GLubyte* version = glGetString(GL_VERSION);
	cout << "Renderer: " << renderer << endl;
	cout << "OpenGL version supported " << version << endl;

	//tell GL to only draw onto a pixel if the shape is closer to the viewer
	glEnable(GL_DEPTH_TEST); //enable depth testing
	glDepthFunc(GL_LESS); //Depth testing interprets a smaller value as 'closer'

	bool wireframe = false;
	if (argc == 2) {
		cout << "Arg[1]: " << argv[1] << endl;
		string check = string(argv[1]);
		if (check.find("-w") != string::npos)
			wireframe = true;
	}

	//wireframe mode
	if (wireframe == true)
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	//Backface culling and setting front counter clockwise
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glFrontFace(GL_CCW);

	return window;
}

void glfw_error_callback(int error, const char* description) {
	cout << "GLFW ERROR: Code " << error << "msg: " << description << endl;
}