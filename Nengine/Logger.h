#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <time.h>
#include <GL\glew.h>
#include <GLFW\glfw3.h>

using namespace std;

class Logger
{
public:
	static Logger* getInstance();
	void gl_log(const string message);
	void gl_log_err(const string message);
	void gl_log_params();
	void print_all(GLuint program);
	bool is_program_valid(GLuint program);
	void _print_program_info_log(GLuint program_index);
	const char* GL_type_to_string(GLenum type);

	~Logger();
private:
	Logger();
	static Logger* log;
	string logfile = "nengine.log";
};

