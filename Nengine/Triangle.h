#pragma once
#include <GL/glew.h>
#include "Math.h"
#include "ShaderManager.h"

class Triangle
{
public:
	Triangle();
	Triangle(float width, float height, const vec3& world_position);
	void render();
	~Triangle();
private:
	float width;
	float height;
	int number_elements;
	GLuint vao;
	GLuint program;
	GLuint model_mat_loc;

	mat4 model_mat;
};

