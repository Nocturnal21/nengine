#include "Logger.h"

Logger* Logger::log = 0;
Logger* Logger::getInstance() {
	if (log == 0)
		log = new Logger();

	return log;
}

void Logger::gl_log(const string message)
{
	ofstream logstream;
	logstream.open(logfile, ios::app);
	if (logstream.is_open()) {
		logstream << message << endl;
		logstream.close();
	}
}

Logger::Logger()
{
	ofstream logstream(logfile);
	if (logstream.is_open()) {
		logstream << "Nengine log:" << endl;
		time_t t;
		time(&t);
		char buf[80];
		ctime_s(buf, sizeof(buf), &t);
		logstream << "Date/Time: " << buf << endl;
		logstream.close();
	}
	else {
		cout << "unable to create logfile." << endl;
	}
}


void Logger::gl_log_err(const string message)
{
	ofstream logstream;
	logstream.open(logfile, ios::app);
	if (logstream.is_open()) {
		logstream << "ERROR: " << message << endl;
		logstream.close();
	}
}

void Logger::gl_log_params()
{
		GLenum params[] = {
			GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS,
			GL_MAX_CUBE_MAP_TEXTURE_SIZE,
			GL_MAX_DRAW_BUFFERS,
			GL_MAX_FRAGMENT_UNIFORM_COMPONENTS,
			GL_MAX_TEXTURE_IMAGE_UNITS,
			GL_MAX_TEXTURE_SIZE,
			GL_MAX_VARYING_FLOATS,
			GL_MAX_VERTEX_ATTRIBS,
			GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS,
			GL_MAX_VERTEX_UNIFORM_COMPONENTS,
			GL_MAX_VIEWPORT_DIMS,
			GL_STEREO
		};

		const string names[] = {
			"GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS",
			"GL_MAX_CUBE_MAP_TEXTURE_SIZE",
			"GL_MAX_DRAW_BUFFERS",
			"GL_MAX_FRAGMENT_UNIFORM_COMPONENTS",
			"GL_MAX_TEXTURE_IMAGE_UNITS",
			"GL_MAX_TEXTURE_SIZE",
			"GL_MAX_VARYING_FLOATS",
			"GL_MAX_VERTEX_ATTRIBS",
			"GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS",
			"GL_MAX_VERTEX_UNIFORM_COMPONENTS",
			"GL_MAX_VIEWPORT_DIMS",
			"GL_STEREO"
		};

		gl_log("GL Context Params:");
		for (int i = 0; i < 10; ++i) {
			int v = 0;
			glGetIntegerv(params[i], &v);
			gl_log(names[i] + " " + to_string(v));
		}

		//others: viewport dimension & stereoskopie
		int v[2];
		v[0] = v[1] = 0;
		glGetIntegerv(params[10], v);
		gl_log(names[10] + " " + to_string(v[0]) + " " + to_string(v[1]));
		unsigned char s = 0;
		glGetBooleanv(params[11], &s);
		gl_log(names[11] + " " + to_string((unsigned int)s));
		gl_log("-----------------------------------\n");

}

void Logger::print_all(GLuint program) {
	printf("-----------------\nshader program %i info:\n", program);
	int params = -1;
	glGetProgramiv(program, GL_LINK_STATUS, &params);
	printf("GL_LINK_STATUS = %i\n", params);

	glGetProgramiv(program, GL_ATTACHED_SHADERS, &params);
	printf("GL_ATTACHED_SHADERS = %i\n", params);

	glGetProgramiv(program, GL_ACTIVE_ATTRIBUTES, &params);
	printf("GL_ACTIVE_ATTRIBUTES = %i\n", params);

	for (GLuint i = 0; i < (GLuint)params; i++) {
		char name[64];
		int max_length = 64;
		int actual_length = 0;
		int size = 0;
		GLenum type;
		glGetActiveAttrib(program, i, max_length, &actual_length, &size, &type, name);
		if (size > 1) {
			for (int j = 0; j < size; j++) {
				char long_name[64];
				sprintf_s(long_name, "%s[%i]", name, j);
				int location = glGetAttribLocation(program, long_name);
				printf("   %i) type:%s name:%s location:%i\n", i, GL_type_to_string(type), long_name, location);
			}
		}
		else {
			int location = glGetAttribLocation(program, name);
			printf("   %i) type:%s name:%s location:%i\n", i, GL_type_to_string(type), name, location);
		}
	}
	glGetProgramiv(program, GL_ACTIVE_UNIFORMS, &params);
	printf("GL_ACTIVE_UNIFORMS = %i\n", params);
	for (GLuint i = 0; i < (GLuint)params; ++i) {
		char name[64];
		int max_length = 64;
		int actual_length = 0;
		int size = 0;
		GLenum type;
		glGetActiveUniform(program, i, max_length, &actual_length, &size, &type, name);

		if (size > 1) {
			for (int j = 0; j < size; j++) {
				char long_name[64];
				sprintf_s(long_name, "%s[%i]", name, j);
				int location = glGetUniformLocation(program, long_name);
				printf("   %i) type:%s name:%s location:%i\n", i, GL_type_to_string(type), long_name, location);
			}
		}
		else {
			int location = glGetUniformLocation(program, name);
			printf("   %i) type:%s name:%s location:%i\n", i, GL_type_to_string(type), name, location);
		}
	}
	//_print_program_info_log(program);
}

const char* Logger::GL_type_to_string(GLenum type) {
	switch (type) {
	case GL_BOOL: return "bool";
	case GL_INT: return "int";
	case GL_FLOAT: return "float";
	case GL_FLOAT_VEC2: return "vec2";
	case GL_FLOAT_VEC3: return "vec3";
	case GL_FLOAT_VEC4: return "vec4";
	case GL_FLOAT_MAT2: return "mat2";
	case GL_FLOAT_MAT3: return "mat3";
	case GL_FLOAT_MAT4: return "mat4";
	case GL_SAMPLER_2D: return "sampler2D";
	case GL_SAMPLER_3D: return "sampler3D";
	case GL_SAMPLER_CUBE: return "samplerCube";
	case GL_SAMPLER_2D_SHADOW: return "sampler2DShadow";
	default: break;
	}
	return "other";
}

bool Logger::is_program_valid(GLuint program) {
	glValidateProgram(program);
	int params = -1;
	glGetProgramiv(program, GL_VALIDATE_STATUS, &params);
	printf("Program %i GL_VALIDATE_STATUS = %i\n", program, params);
	if (GL_TRUE != params) {
		_print_program_info_log(program);
		return false;
	}
	return true;
}

void Logger::_print_program_info_log(GLuint program_index) {
	int max_length = 2048;
	int actual_length = 0;
	char log[2048];
	glGetShaderInfoLog(program_index, max_length, &actual_length, log);
	printf("Program info log for GL index %u:\n%s\n", program_index, log);
}


Logger::~Logger()
{
}
