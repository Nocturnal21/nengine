#include "Environment.h"

Environment::Environment(GLFWwindow * window, int width, int height)
{
	this->window = window;
	window_width = width;
	window_height = height;
	aspect_ratio = (float)width / (float)height;

	glfwSetWindowSizeCallback(window, glfw_window_size_callback);
	logger = Logger::getInstance();
	sh_manager = ShaderManager::getInstance();

	//Game objects
	tri1 = new Triangle(2.0f, 3.0f, vec3(0.0f, 0.0f, -10.0f));
	tri2 = new Triangle(2.0f, 3.0f, vec3(10.0f, 0.0f, -5.0f));
	tri3 = new Triangle(2.0f, 3.0f, vec3(-10.0f, 0.0f, -5.0f));

	camera = new FPSCamera(window, aspect_ratio, vec3(0.0f, 0.0f, 8.0f), vec3(0.0f, 0.0f, -1.0f));

	//check program status
	logger->print_all(sh_manager->getShaderProgram("program_default"));
}

Environment::~Environment()
{
}

void Environment::start()
{
	loop();
}

void Environment::loop()
{
	double current_time = glfwGetTime();
	double accumulator = 0.0;

	while (!glfwWindowShouldClose(window)) {
		double new_time = glfwGetTime();
		update_fps_counter(new_time);
		double frame_time = new_time - current_time;
		current_time = new_time;

		accumulator += frame_time;

		while (accumulator >= dt)
		{
			update(dt);

			accumulator -= dt;
			t += dt;
		}

		render();
	}
}

void Environment::update(double time_delta) {
	//do something
	camera->update(time_delta);
}

void Environment::render() {
	//clear the drawing surface
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glViewport(0, 0, window_width, window_height);

	//draw gameobjects
	tri1->render();
	tri2->render();
	tri3->render();

	//update other events like inpute handling
	glfwPollEvents();
	//put the stuff we've been drawing onto the surface
	glfwSwapBuffers(window);
}

void Environment::update_fps_counter(double current_time) {
	static double previous_time = 0;
	static int frame_count = 0;
	double elapsed_seconds = current_time - previous_time;

	if (elapsed_seconds > 0.5)
	{
		previous_time = current_time;
		double fps = (double)frame_count / elapsed_seconds;
		std::string title = "Nengine @ fps: " + std::to_string(fps);
		//std::cout << title << std::endl;
		glfwSetWindowTitle(window, title.c_str());
		frame_count = 0;
	}
	++frame_count;
}

void glfw_window_size_callback(GLFWwindow* window, int width, int height) {
	window_width = width;
	window_height = height;
}