#include "ShaderManager.h"

ShaderManager* ShaderManager::instance = NULL;

ShaderManager* ShaderManager::getInstance() {
	if (instance == NULL) {
		instance = new ShaderManager();
	}
	return instance;
}

const GLuint ShaderManager::getShaderProgram(std::string program_name) {
	GLuint p = GL_FALSE;
	p = program_map[program_name];
	return p;
}

ShaderManager::ShaderManager() {
	logger = Logger::getInstance();
	load_shaders();
	link_shaders();
}

void ShaderManager::load_shaders() {
	string name;
	ifstream input("Shader/shaders.list");

	if (input.is_open()) {
		while (getline(input, name))
		{
			cout << "Shader name to process: " << name << endl;
			GLenum shader_type;
			if (name.find(".vert") != string::npos)
				shader_type = GL_VERTEX_SHADER;
			else if (name.find(".frag") != string::npos)
				shader_type = GL_FRAGMENT_SHADER;
			else
				shader_type = GL_FALSE;
			if (shader_type != GL_FALSE) {
				string shader_path = "Shader/";
				string shader_src;
				shader_path.append(name);
				cout << "Trying to open: " << shader_path << endl;
				if (read_shader(shader_path, &shader_src) == true) {
					cout << "Shader name: " << name << endl;
					cout << shader_src << endl;

					//TODO: compile and link shader
					GLuint sh = glCreateShader(shader_type);
					const char* shader_src_c = shader_src.c_str();
					glShaderSource(sh, 1, &shader_src_c, NULL);
					glCompileShader(sh);

					int status = -1;
					glGetShaderiv(sh, GL_COMPILE_STATUS, &status);
					if (status != GL_TRUE)
						cerr << "ERROR: Could not compile shader index: " << sh << endl;
					else
						cout << "Successfully compiled shader index: " << sh << endl;
					shader_map[name] = sh;
				}
				else
					cout << "Could not read shader source of: " << name << endl;
			}
			else
				cout << "Found: Not valid shader file named: " << name << endl;
		}
		input.close();
	}
	else
		cerr << "ERROR: Could not open shader.list" << endl;
}

void ShaderManager::link_shaders() {
	int status = -1;

	//default shader 
	GLuint program_default = glCreateProgram();
	glAttachShader(program_default, shader_map["default.vert"]);
	glAttachShader(program_default, shader_map["default.frag"]);
	glLinkProgram(program_default);
	glGetProgramiv(program_default, GL_LINK_STATUS, &status);
	if (GL_TRUE != status) {
		(void)fprintf(stderr, "ERROR: Could not link shader program GL index %u\n", program_default);
		_print_program_info_log(program_default);
	}
	else {
		cout << "Successfully linked: program_default" << endl;
		program_map["program_default"] = program_default;
	}
}

bool ShaderManager::read_shader(const string &dir, string *shader_src) {
	string line;
	ifstream input(dir);
	if (input.is_open()) {
		while (getline(input, line))
		{
			shader_src->append(line).append("\n");
		}
		input.close();
		return true;
	}
	else {
		cout << "Unable to open file: " << dir << endl;
		return false;
	}
}

void ShaderManager::_print_shader_info_log(GLuint shader_index) {
	int max_length = 2048;
	int actual_length = 0;
	char log[2048];
	glGetShaderInfoLog(shader_index, max_length, &actual_length, log);
	printf("Shader info log for GL index %u:\n%s\n", shader_index, log);
}


void ShaderManager::_print_program_info_log(GLuint program_index) {
	int max_length = 2048;
	int actual_length = 0;
	char log[2048];
	glGetShaderInfoLog(program_index, max_length, &actual_length, log);
	printf("Program info log for GL index %u:\n%s\n", program_index, log);
}
