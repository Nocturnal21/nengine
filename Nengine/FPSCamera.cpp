#include "FPSCamera.h"

void FPSCamera::glfw_key_callback(GLFWwindow * window, int key, int scancode, int action, int mods)
{
	if (action == GLFW_PRESS || action == GLFW_REPEAT) {
		switch (key) {
		case GLFW_KEY_W:
			w_key = true;
			break;
		case GLFW_KEY_S:
			s_key = true;
			break;
		case GLFW_KEY_A:
			a_key = true;
			break;
		case GLFW_KEY_D:
			d_key = true;
			break;
		case GLFW_KEY_ESCAPE:
			glfwSetWindowShouldClose(window, GL_TRUE);
		}
	}
	else {
		switch (key) {
		case GLFW_KEY_W:
			w_key = false;
			break;
		case GLFW_KEY_S:
			s_key = false;
			break;
		case GLFW_KEY_A:
			a_key = false;
			break;
		case GLFW_KEY_D:
			d_key = false;
			break;
		}
	}
}

void FPSCamera::glfw_cursor_callback(GLFWwindow * window, double xpos, double ypos)
{
	static double dx = 0, dy = 0;
	static double prev_x = 1280, prev_y = 720;
	
	dx = prev_x - xpos;
	dy = prev_y - ypos;
	prev_x = xpos;
	prev_y = ypos;

	pitch -= dy;
	yaw -= dx;
}

FPSCamera::FPSCamera(GLFWwindow * window, float aspect_ratio, vec3 world_position, vec3 lookat)
{
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	this->position = world_position;
	this->window = window;
	program = ShaderManager::getInstance()->getShaderProgram("program_default");

	projection_matrix = perspective(67.0f, aspect_ratio, 0.1f, 100.0f);

	//init rotation => no rotation
	rotation_versor = versor(1.0f, 0.0f, 0.0f, 0.0f);
	
	//getting initial view mat
	mat4 view_matrix = quat_to_mat4(rotation_versor) * translate(identity_mat4(), position * -1);
	mat4 model_matrix = identity_mat4();

	view_mat_loc = glGetUniformLocation(program, "view");
	model_mat_loc = glGetUniformLocation(program, "model");
	proj_mat_loc = glGetUniformLocation(program, "proj");

	glUseProgram(program);
	glUniformMatrix4fv(view_mat_loc, 1, GL_FALSE, view_matrix.m);
	glUseProgram(program);
	glUniformMatrix4fv(model_mat_loc, 1, GL_FALSE, model_matrix.m);
	glUseProgram(program);
	glUniformMatrix4fv(proj_mat_loc, 1, GL_FALSE, projection_matrix.m);

	glfwSetKeyCallback(window, glfw_key_callback);
	glfwSetCursorPosCallback(window, glfw_cursor_callback);
}


FPSCamera::~FPSCamera()
{
}

void FPSCamera::update(double time_delta)
{
	bool moved = false;
	if (yaw != 0) {
		rotate_y(float(yaw / mouse_sensitivity_yaw * time_delta));
		moved = true;
	}
	if (pitch != 0) {
		rotate_x(float(pitch / mouse_sensitivity_pitch * time_delta));
		moved = true;
	}

	if (w_key != s_key) {
		float dir;
		if (w_key)
			dir = -1.0f;
		else
			dir = 1.0f;
		movez(dir * time_delta * cam_speed);
		moved = true;
	}

	if (a_key != d_key) {
		float dir;
		if (d_key)
			dir = -1.0f;
		else
			dir = 1.0f;
		movex(dir * time_delta * cam_speed);
		moved = true;
	}

	if (moved) {
		// translation
		mat4 T = translate(identity_mat4(), vec3(-position.v[0], -position.v[1], -position.v[2]));

		// rotation
		mat4 R = quat_to_mat4(rotation_versor);		

		// applying to uniform
		mat4 V = R * T;
		glUniformMatrix4fv(model_mat_loc, 1, GL_FALSE, identity_mat4().m);
		glUniformMatrix4fv(view_mat_loc, 1, GL_FALSE, V.m);

		moved = false;
		yaw = pitch = 0.0f;
	}
}

void FPSCamera::movex(float xmmod)
{
	vec3 axis = vec3(1.0f, 0.0f, 0.0f);
	versor vec_versor = versor(0.0f, axis.v[0], axis.v[1], axis.v[2]);
	versor res_versor = rotation_versor * vec_versor * conjugate(rotation_versor);
	vec3 res_vec = vec3(res_versor.q[1], res_versor.q[2], res_versor.q[3]);
	res_vec.v[0] = -res_vec.v[0];
	res_vec.v[1] = -res_vec.v[1];

	position += res_vec * xmmod;
}

void FPSCamera::movez(float zmmod)
{
	vec3 axis = vec3(0.0f, 0.0f, 1.0f);
	versor vec_versor = versor(0.0f, axis.v[0], axis.v[1], axis.v[2]);
	versor res_versor = rotation_versor * vec_versor * conjugate(rotation_versor);
	vec3 res_vec = vec3(res_versor.q[1], res_versor.q[2], res_versor.q[3]);
	res_vec.v[0] = -res_vec.v[0];
	res_vec.v[1] = -res_vec.v[1];

	position += res_vec * zmmod;
}

void FPSCamera::rotate_x(float factor) {
	static float total_rot = 0.0f;
	total_rot += factor;
	if (total_rot == 1) {
		return;
	}
	else if (total_rot == -1) {
		return;
	}
	versor temp_rot = quat_from_axis_rad(factor, 1.0f, 0.0f, 0.0f);
	rotation_versor = rotation_versor * temp_rot;
}

void FPSCamera::rotate_y(float factor) {
	versor temp_rot = quat_from_axis_rad(factor, 0.0f, 1.0f, 0.0f);
	rotation_versor = temp_rot * rotation_versor;
}

bool FPSCamera::w_key = false;
bool FPSCamera::s_key = false;
bool FPSCamera::a_key = false;
bool FPSCamera::d_key = false;

double FPSCamera::yaw = 0.0f;
double FPSCamera::pitch = 0.0f;